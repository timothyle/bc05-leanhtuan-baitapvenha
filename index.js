const BASE_URL = "https://636652cd046eddf1bafd7a48.mockapi.io/";

var idEdited = null;

function fetchAllName (){
    // render all todos service
    turnOnLoading();
    axios({
    url: `${BASE_URL}/BaiTapVeNha` ,
    method:"GET",
})
.then(function(res){
    turnOffLoading();
    renderName(res.data);
})
.catch(function(err){
    turnOffLoading();
    console.log("err:",err);
});
}
// chạy lần đầu khi load trang
fetchAllName();
// remove todo service
function removeName(idBaiTapVeNha){
    turnOnLoading();
    axios({
        url: `${BASE_URL}/BaiTapVeNha/${idBaiTapVeNha}`,method: "DELETE",
    })
    .then(function(res){
        
        // gọi lại api get all todos
        turnOffLoading();
        fetchAllName();
        
        console.log("res:",res);
    })
    .catch(function(err){
        turnOffLoading();
        console.log("err:",err);
    });
}
function addName(){
   var data = layThongTinTuForm();
    var newName = {
        firstName: data.firstName,
        lastName: data.lastName,
        desc: data.desc,
        isVerified: true,
    };
    turnOnLoading();
    axios({
        url: `${BASE_URL}/BaiTapVeNha`,
        method:"POST",
        data:newName,
    })
    .then(function(res){
        // gọi lại api get all todos
        turnOffLoading();
        fetchAllName();
        
        console.log("res:",res);
    })
    .catch(function(err){
        turnOffLoading();
        console.log("err:",err);
    });
}

function editName(idBaiTapVeNha){
    turnOnLoading();
    axios({
        url: `${BASE_URL}/BaiTapVeNha/${idBaiTapVeNha}`,
        method:"GET",
    })
    .then(function (res){
        turnOffLoading();
        document.getElementById("firstName").value = res.data.firstName;
        document.getElementById("lastName").value = res.data.lastName;
        document.getElementById("desc").value = res.data.desc;
        idEdited = res.data.id;
    })
    .catch(function (err) {
        turnOffLoading();
        console.log("error:",err);
        document.getElementById("message").innerHTML = `<span class="txt-danger">Lỗi</span>`;
    })
}

function updateName(){
    turnOnLoading();
    let data = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/BaiTapVeNha/${idEdited}`,
        method:"PUT",
        data:data,
    })
    .then(function(res){
        console.log(res);
        fetchAllName();
        turnOffLoading();
    })
    .catch(function(err){
        turnOffLoading();
        document.getElementById("message").innerHTML = `<span class="txt-danger">Lỗi</span>`;
        console.log(err);
    })
}