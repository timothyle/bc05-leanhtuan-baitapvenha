function renderName(BaiTapVeNha){
    var contentHTML="";
    BaiTapVeNha.forEach(function(item){
        var contentTr =`<tr>
        <td>${item.id}</td>
        <td>${item.firstName}</td>
        <td>${item.lastName}</td>
        <td>${item.desc}</td>
        <td><input type="checkbox" ${item.isVerified? "checked" : ""}></td>
        <td><button onclick="removeName(${item.id})" class="btn btn-warning">Delete</button></td>
        <td>
        <button onclick="editName(${item.id})" class="btn btn-info">Sửa</button>
        </td>
        </tr>`;
        contentHTML += contentTr
    });
    document.getElementById("tbody-tbName").innerHTML = contentHTML;
};

function turnOnLoading(){
    document.getElementById("loading").style.display = 
"flex";
}

function turnOffLoading(){
    document.getElementById("loading").style.display = 
"none";
}

function layThongTinTuForm(){
    var firstName = document.getElementById("firstName").value;
    var lastName = document.getElementById("lastName").value;
    var desc = document.getElementById("desc").value;
    return {
        firstName: firstName,
        lastName: lastName,
        desc: desc,
    }
}
